from pipeline.mainClass import mainClass
import itertools
import nbformat as nbf

class selection():

    def getArgsRFile(self,x,cwdPath):
        filePath = cwdPath+"/pipeline/Rscripts/"+ x + ".R"
        f = open(filePath,"r")
        for s in f:
            if s.startswith("perform_"+x):
                s = s.split("(")
                s = s[1].split(")")
                s = s[0].split(",")
                for i in range(len(s)):
                    s[i] = s[i].strip()
                break
        if isinstance(s,list) is False:
            raise ValueError("File "+x+".R does not contain appropriate function definition")
        return s

    def putPython(self):
        a="df_RPy = temp.df"
        return a

    def putR(self):
        a="%put df_RPy\n"
        return a

    def getR(self):
        a="%get df_RPy\n%get metadata\n"
        return a
    
    def getPythonWF(self):
        a="temp.df = df_RPy"
        return a
    
    def getArgsRWF(self,x,params,operation):
        s=[]
        for it in x:
            try:
                if it=='df_RPy':
                    continue
                if it=='metadata':
                    s.append(it+" = "+it)
                    continue
                z=params[it]
                if(isinstance(z,str)):
                    s.append(it+" = '"+str(z)+"'")
                else:
                    s.append(it+" = "+str(z))
            except:
                raise KeyError(it+" is compulsory argument for "+operation)
        a="df_RPy = df_RPy" 
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def getImportStatements(self):
        code="""\
from pipeline.mainClass import mainClass
from pipeline.user import automate
import matplotlib
import plotly
plotly.offline.init_notebook_mode()"""
        return code

    def getMainObject(self,yamlFilePath):
        code="""\
params = automate().getListFromYaml('"""+yamlFilePath+"""')
mainObject = automate().get_data(params)
metadata = mainObject.colMetaData
df = mainObject.df"""
        return code

    def getDriveGCT(self,yamlFilePath,x):
        cells = []
        code="""\
library('googledrive')
td <- team_drive_get('"""+x[0]+"""')
file_drive_path <- file.path( '"""
        code+=x[1]+"'"
        for i in range(2,len(x)):
            code+=", '"+x[i]+"'"
        code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file <- drive_download(file_drive_metadata, overwrite = T)"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "R"}
        cells.append(cell)
        code="""\
params = automate().getListFromYaml('"""+yamlFilePath+"""')
mainObject = automate().get_data_gct('"""+x[-1]+"""')
metadata = mainObject.colMetaData"""
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "SoS"}
        cells.append(cell)
        return cells

    def getDriveAll(self,yamlFilePath,mainX=None,rowX=None,colX=None):
        cells=[]
        code = "library('googledrive')\ntd <- team_drive_get('"+mainX[0]+"')\n\n"
        if mainX is not None:
            code+="file_drive_path <- file.path( '"
            code+=mainX[1]+"'"
            for i in range(2,len(mainX)):
                code+=", '"+mainX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_main <- drive_download(file_drive_metadata, overwrite = T)\n\n"
        if rowX is not None:
            code+="file_drive_path <- file.path( '"
            code+=rowX[1]+"'"
            for i in range(2,len(rowX)):
                code+=", '"+rowX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_rowMD <- drive_download(file_drive_metadata, overwrite = T)\n\n"
        if colX is not None:
            code+="file_drive_path <- file.path( '"
            code+=colX[1]+"'"
            for i in range(2,len(colX)):
                code+=", '"+colX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_colMD <- drive_download(file_drive_metadata, overwrite = T)"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "R"}
        cells.append(cell)
        code = "params = automate().getListFromYaml('"+yamlFilePath+"')\n"
        a = []
        if mainX is not None:
            a.append("mainCSV = '"+mainX[-1]+"'")
        if rowX is not None:
            a.append("rowCSV = '"+rowX[-1]+"'")
        if colX is not None:
            a.append("colCSV = '"+colX[-1]+"'")
        s = a[0]
        for i in range(1,len(a)):
            s+=", "+a[i]
        code += "mainObject = automate().get_data_all("+s+")\nmetadata = mainObject.colMetaData"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "SoS"}
        cells.append(cell)
        return cells
    
    def getDriveFile(self,yamlFilePath,params):
        gct=None
        mainCSV=None
        rowCSV=None
        colCSV=None
        for key,values in params.items():
            if key=='gct':
                gct=values
            elif key=='mainCSV':
                mainCSV=values
            elif key=='rowCSV':
                rowCSV=values
            elif key=='colCSV':
                colCSV=values
        if gct is not None:
            cells = self.getDriveGCT(yamlFilePath,gct)
        else:
            cells = self.getDriveAll(yamlFilePath,mainX=mainCSV,rowX=rowCSV,colX=colCSV)
        return cells

    def getArgs(self,params):
        code="kwargs=[]"
        for i in range(len(params)):
            code+="\n"+"kwargs.append("+str(params[i])+")"
        return code

    def getFunctions(self,apply,rScripts,pyScripts,cwdPath):
        cells = []
        a = []
        for i in range(len(apply)):
            for j in range(len(apply[i])):
                a.append(apply[i][j])
        apply = set(a)
        apply = list(apply)
        for x in apply:
            if x in rScripts:                
                cells.append(nbf.v4.new_markdown_cell("### Importing function "+x))
                filePath = cwdPath+"/pipeline/Rscripts/"+ x + ".R"
                f = open(filePath,"r")
                s=""
                for x in f:
                    if x.startswith("#"):
                        continue
                    s+=x
                cell=nbf.v4.new_code_cell(s)
                cell['metadata']={"kernel": "R"}
                cells.append(cell)  
            elif x in pyScripts:
                cells.append(nbf.v4.new_markdown_cell("### Importing function "+x))
                filePath = cwdPath+"/pipeline/pyScripts/"+ x + ".py"
                f = open(filePath,"r")
                s=""
                for x in f:
                    if x.startswith("#"):
                        continue
                    s+=x
                cell=nbf.v4.new_code_cell(s)
                cell['metadata']={"kernel": "SoS"}
                cells.append(cell)  
            else:
                raise KeyError("Function "+x+" neither in R nor in python folder")
        return cells
    
    def generateNotebook(self,paramsData,apply,params,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath):
        nPermutations = len(apply)
        nOps = len(apply[0])
        nb=nbf.v4.new_notebook()
        nb['metadata']={
            "kernelspec": {
                "display_name": "SoS",
                "language": "sos",
                "name": "sos"
            },
            "language_info": {
                "codemirror_mode": "sos",
                "file_extension": ".sos",
                "mimetype": "text/x-sos",
                "name": "sos",
                "nbconvert_exporter": "sos_notebook.converter.SoS_Exporter",
                "pygments_lexer": "sos"
            }
        }
        cells=[]
        cells.append(nbf.v4.new_markdown_cell("### Importing libraries"))
        cells.append(nbf.v4.new_code_cell(self.getImportStatements()))
        cells.append(nbf.v4.new_code_cell("%matplotlib inline"))
        cells.append(nbf.v4.new_markdown_cell("### Setting things up...(Importing data and other necessities)"))
        for key,values in paramsData.items():
            if key=='data':
                if paramsData['data']['from'] =='disk':
                    cells.append(nbf.v4.new_code_cell(self.getMainObject(yamlFilePath)))
                elif paramsData['data']['from'] =='drive':
                    moreCells = self.getDriveFile(yamlFilePath,paramsData['data'])
                    for x in moreCells:
                        cells.append(x)
                else:
                    raise ValueError("data['from'] should either be 'disk' or 'drive'")
        cells.append(nbf.v4.new_code_cell(self.getArgs(params)))
        temp = self.getFunctions(apply,rScripts,pyScripts,cwdPath)
        for x in temp:
            cells.append(x)
        for i in range(nPermutations):
            s="### Selection over "+apply[i][0]
            for j in range(1,nOps):
                s+=", "+apply[i][j]
            cells.append(nbf.v4.new_markdown_cell(s))
            apply[i] = list(apply[i])
            code = "temp = mainObject\ntemp.df = df"
            cells.append(nbf.v4.new_code_cell(code))
            for j in range(nOps):
                if apply[i][j] in rScripts:
                    args = self.getArgsRFile(apply[i][j],cwdPath)
                    code=self.putPython()
                    cells.append(nbf.v4.new_code_cell(code))
                    code=self.getR()+self.putR()
                    code+="df_RPy = perform_"+apply[i][j]+"("+self.getArgsRWF(args,params[j],apply[i][j])+")"
                    cell=nbf.v4.new_code_cell(code)
                    cell['metadata']={"kernel": "R"}
                    cells.append(cell)  
                    code=self.getPythonWF()
                    cell=nbf.v4.new_code_cell(code)
                    cell['metadata']={"kernel": "SoS"}
                    cells.append(cell)                 
                else:
                    code=""
                    # if(parent_list[j]=='visualise' and self.inline==False):
                    #     self.inline=True
                    #     code+="%matplotlib inline\n"
                    code+=apply[i][j]+"(temp, **kwargs["+str(j)+"])"
                    cells.append(nbf.v4.new_code_cell(code))
        nb['cells']=cells
        nbf.write(nb,notebookName)
        return

    def buildNet(self,params,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath):
        paramsData = params[0]
        del params[0]
        del params[0]
        net = []
        args = []
        for x in params:
            for key,values in x.items():
                net.append(values['over'])
                del values['over']
                args.append(values)
        apply = list(itertools.product(*net))
        self.generateNotebook(paramsData,apply,args,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath)
            