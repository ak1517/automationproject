import pandas as pd

def merge(data1,data2,**kwargs):
    df1 = data1.df
    df2 = data2.df
    meta1 = data1.colMetaData
    meta2 = data2.colMetaData
    mergeObject = mainClass()
    mergeObject.df = df1.merge(df2,how = 'outer',left_on = df1.index, right_on = df2.index)
    mergeObject.df = mergeObject.df.set_index('key_0')
    meta1['batch'] = 1
    meta2['batch'] = 2
    mergeObject.colMetaData = meta1.append(meta2)
    return mergeObject