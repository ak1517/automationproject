import matplotlib.pyplot as plt 
from pipeline.mainClass import mainClass
import pandas as pd
from pipeline.pyScripts.outlier_detect import outlier_detect

def intellify(df):
    anomaly_df = pd.DataFrame()
    anomaly_df['mean'] = list(df.mean())
    anomaly_df['median'] = list(df.median())
    anomaly_df['lowerquantile'] = list(df.quantile(0.25))
    anomaly_df['upperquantile'] = list(df.quantile(0.75))
    anomaly_df['std'] = list(df.std())
    outlier_detect(df,anomaly_df)

def boxPlot(mainObject,**kwargs):
    df=mainObject.df
    columns=None
    for key, value in kwargs.items():
        if key=='columns':
            columns=value
            break
    if columns is not None: 
        boxplot = df.boxplot(column=columns)
    else:
        boxplot = df.boxplot()
    # intellify(df)
    plt.show(boxplot)
