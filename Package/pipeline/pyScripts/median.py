import pandas as pd
from pipeline.mainClass import mainClass

def median(mainObject,**kwargs):
    data = mainObject.df
    new_data = data.apply(lambda x:(x - data.median(axis=0))/data.mad(axis=0),axis=1)
    mainObject.df = new_data
    return new_data