import plotly
import numpy as np
from pipeline.mainClass import mainClass
from plotly.graph_objs import Scatter, Layout

def volcanoPlot(mainObject,feature,x,y,**kwargs):
    df = mainObject.df
    df = df[df['P.Value'] != 0]
    df['significant']=np.where(df[feature]<=0.05,'yes','no')
    unique_val = list(df['significant'].unique())
    plotly.offline.iplot({
        'data': [   
            {
                'x': df[df['significant']==val][x],
                'y': df[df['significant']==val][y],
                'name': str(val), 'mode': 'markers',
            } for val in unique_val
        ],
        'layout': {
            'xaxis': {'title': x},
            'yaxis': {'title':"-log10 " + y}
        }
    })
