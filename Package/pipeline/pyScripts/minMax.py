import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from pipeline.mainClass import mainClass

def minMax(mainObject,**kwargs):
    df = mainObject.df
    rowname = list(df.index)
    colnames = list(df.columns)
    x= df.values
    scaler = MinMaxScaler()
    rescaled_arr = scaler.fit_transform(x)
    rescaled_df = pd.DataFrame(rescaled_arr,columns=colnames,index=rowname)
    mainObject.df = rescaled_df
    return rescaled_df