import pandas as pd
import numpy as np
from pipeline.mainClass import mainClass

def quantile(mainObject,**kwargs):
    df = mainObject.df
    dic = {}
    for col in df:
        dic.update({col : sorted(df[col])})
    sorted_df = pd.DataFrame(dic)
    rank = sorted_df.mean(axis = 1).tolist()
    for col in df:
        t = np.searchsorted(np.sort(df[col]), df[col])
        df[col] = [rank[i] for i in t]
    return df