import http.client
import json
import pandas as pd
import subprocess,os,json
from pipeline.mainClass import mainClass
import plotly as py
import numpy as np
import plotly.graph_objs as go
from plotly import tools

def x2k(mainObject, options={},**kwargs):
    # Open HTTP connection
    data = mainObject.df
    input_genes = (data.index)
    # print(type(input_genes))
    # print(type(str(input_genes)))
    data.to_csv("x2k_in.csv")
    
    conn = http.client.HTTPConnection("amp.pharm.mssm.edu")
    # Set default options
    default_options = {'text-genes': '\n'.join(input_genes),
                    'included_organisms': 'both',
                    'TF-target gene background database used for enrichment': 'ChEA & ENCODE Consensus',
                    'sort transcription factors by': 'p-value',
                    'min_network_size': 10,
                    'number of top TFs': 10,
                    'path_length': 2,
                    'min_number_of_articles_supporting_interaction': 0,
                    'max_number_of_interactions_per_protein': 200,
                    'max_number_of_interactions_per_article': 100,
                    'enable_BioGRID': True,
                    'enable_IntAct': True,
                    'enable_MINT': True,
                    'enable_ppid': True,
                    'enable_Stelzl': True,
                    'kinase interactions to include': 'kea 2018',
                    'sort kinases by': 'p-value'}

    # Update options
    for key, value in options.items():
        if key in default_options.keys() and key != 'text-genes':
            default_options.update({key: value})

    # Get payload
    boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
    payload = ''.join(
        ['--' + boundary + '\r\nContent-Disposition: form-data; name=\"{key}\"\r\n\r\n{value}\r\n'.format(**locals())
        for key, value in default_options.items()]) + '--' + boundary + '--'

    # Get Headers
    headers = {
        'content-type': "multipart/form-data; boundary=" + boundary,
        'cache-control': "no-cache",
    }

    # Initialize connection
    conn.request("POST", "/X2K/api", payload, headers)

    # Get response
    res = conn.getresponse()

    # Read response
    data = res.read().decode('utf-8')

    # Convert to dictionary
    x2k_results = {key: json.loads(value) if key != 'input' else value for key, value in json.loads(data).items()}

    # Clean results
    x2k_results['ChEA'] = x2k_results['ChEA']['tfs']
    x2k_results['G2N'] = x2k_results['G2N']['network']
    x2k_results['KEA'] = x2k_results['KEA']['kinases']
    x2k_results['X2K'] = x2k_results['X2K']['network']

    chea_dataframe = pd.DataFrame(x2k_results['ChEA'])
    G2N_edges_dataframe = pd.DataFrame(x2k_results['G2N']['interactions'])
    KEA_dataframe = pd.DataFrame(x2k_results['KEA'])
    X2K_nodes_dataframe = pd.DataFrame(x2k_results['X2K']['nodes']).drop('pvalue', axis=1)
    x2k_edges_dataframe = pd.DataFrame(x2k_results['X2K']['interactions'])

    chea_dataframe.to_csv("results/chea_results.csv")
    G2N_edges_dataframe.to_csv("results/g2n_edges.csv")
    KEA_dataframe.to_csv("results/kea_results.csv")
    X2K_nodes_dataframe.to_csv("results/X2K_nodes.csv")
    x2k_edges_dataframe.to_csv("results/x2k_edges.csv")

    # print(chea_dataframe['pvalue'].head())
    chea_dataframe.sort_values(["pvalue"], axis=0, 
                ascending=False, inplace=True)
    # print(chea_dataframe['pvalue'].head())
    KEA_dataframe.sort_values(["pvalue"], axis=0, 
                ascending=False, inplace=True)
    data2 = go.Bar(
        y=KEA_dataframe['name'],
        x=- np.log10( KEA_dataframe['pvalue']),
        orientation = 'h'
    )
    data1 = go.Bar(
        y=chea_dataframe['simpleName'],
        x=- np.log10( chea_dataframe['pvalue']),
        orientation = 'h'
    )

    fig = tools.make_subplots(rows=1, cols=2,subplot_titles=('CHEA', 'KEA'))
    fig.append_trace(data1, 1, 1)
    fig.append_trace(data2, 1, 2)
    fig['layout']['xaxis1'].update(title='-log10 Pvalue')
    fig['layout']['xaxis2'].update(title='-log10 Pvalue')
    py.offline.iplot(fig ,filename='horizontal-bar.html')

