import pandas as pd
from pipeline.mainClass import mainClass
import numpy as np

def logNorm(mainObject,**kwargs):
    df = mainObject.df
    df_log_norm = df.applymap(np.log2)
    mainObject.df = df_log_norm
    return df_log_norm