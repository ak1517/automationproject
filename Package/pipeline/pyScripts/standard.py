import pandas as pd
from sklearn.preprocessing import StandardScaler
from pipeline.mainClass import mainClass

def standard(mainObject,**kwargs):
    df = mainObject.df
    rowname = list(df.index)
    colnames = list(df.columns)
    scaler = StandardScaler().fit(df)
    rescaled_arr = scaler.transform(df)
    rescaled_df = pd.DataFrame(rescaled_arr,columns=colnames,index=rowname)
    mainObject.df = rescaled_df
    return rescaled_df