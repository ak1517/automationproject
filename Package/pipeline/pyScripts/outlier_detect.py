    
from scipy import stats
import numpy as np
def outlier_detect(df,anomaly_df,threshold=2.5):
    val = np.abs(stats.zscore(anomaly_df)).mean()
    arr = []
    new_Arr=[]
    for i in range(0,anomaly_df.shape[0]):
        arr.append((np.abs(stats.zscore(anomaly_df))[[i]].mean() < val ))
    df_t = anomaly_df[arr]
    new_z_score = np.abs(stats.zscore(df_t)).mean()
    for i in range(0,anomaly_df.shape[0]):
        new_Arr.append((np.abs(stats.zscore(anomaly_df))[[i]].mean() < threshold*new_z_score ))
    for i in range(0,len(new_Arr)):
        if new_Arr[i] == False:
            # print("Mean zscore is "+str(new_z_score))
            print(df.columns.values[i] + " is an outlier in your data with zscore "+str(np.abs(stats.zscore(anomaly_df))[[i]].mean()))
