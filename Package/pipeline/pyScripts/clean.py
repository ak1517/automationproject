import pandas as pd
import numpy as np
from pipeline.mainClass import mainClass

def clean(mainObject,**kwargs):
    missing=False
    removeDuplicates=False
    subset=False
    for key,values in kwargs.items():
        if key=='missing':
            missing=values
        elif key=='subset':
            subset=values
        elif key=='removeDuplicates':
            removeDuplicates=values
    if missing:
        removeMissing(mainObject,**kwargs)
    if removeDuplicates:
        removeDuplicates(mainObject,**kwargs)
    if subset:
        subset(mainObject,**kwargs)

def is_float(value):
    try:
        temp=float(value)
        return True
    except:
        return False

# cohort column is the metadata column that has cohorts. 
# sample column is the metadata column that has samples names (column names for gene data in most cases)
def removeMissing(mainObject,**kwargs):   
    df=mainObject.df
    meta_data=mainObject.colMetaData 
    cohortColumn=None
    sampleColumn=None
    for key,values in kwargs.items():
        if key=='cohortColumn':
            cohortColumn=values
        elif key=='sampleColumn':
            sampleColumn=values
    if cohortColumn is None or sampleColumn is None:
        raise ValueError("Enter cohort column and sample column")
    df.fillna("No Val",inplace=True)
    columns = df.columns[df.dtypes==object]
    for cl in columns:
        for i in df.index :
            val = df.get_value(i,cl)
            n = is_float(val)
            if not n:
                time = int(meta_data.loc[meta_data[sampleColumn]==cl][cohortColumn])
                similar_time = meta_data.loc[meta_data[cohortColumn]==time][sampleColumn]            
                val1 = df.loc[i,str(similar_time.iloc[0])]
                val2 = df.loc[i,str(similar_time.iloc[1])]
                val3 = df.loc[i,str(similar_time.iloc[2])]
                if is_float(val2) and is_float(val3):
                    val1 = (val3+val2)/2
                    df.set_value(i,str(similar_time.iloc[0]),val1)
                    df.set_value(i,str(similar_time.iloc[1]),val2)
                    df.set_value(i,str(similar_time.iloc[2]),val3)
                elif is_float(val2):
                    val3 = val2
                    val1 = val2
                    df.set_value(i,str(similar_time.iloc[0]),val1)
                    df.set_value(i,str(similar_time.iloc[1]),val2)
                    df.set_value(i,str(similar_time.iloc[2]),val3)
                elif is_float(val3):
                    val2 = val3
                    val1 = val3
                    df.set_value(i,str(similar_time.iloc[0]),val1)
                    df.set_value(i,str(similar_time.iloc[1]),val2)
                    df.set_value(i,str(similar_time.iloc[2]),val3)
                else:
                    df.drop([i])
        df[cl] = pd.to_numeric(df[cl])
    mainObject.df=df
    return df

def subset(mainObject,**kwargs):
    df=mainObject.df
    rowMetaData=mainObject.rowMetaData
    colMetaData=mainObject.colMetaData
    subsetMainData=False
    subsetRowMetaData=False
    subsetColMetaData=False
    for key,values in kwargs.items():
        if key=='subsetMainData':
            subsetMainData=values
        elif key=='subsetRowMetaData':
            subsetRowMetaData=values
        elif key=='subsetColMetaData':
            subsetColMetaData=values
    if subsetMainData:
        df=subsetMain(df,**kwargs)
        mainObject.df=df
    if subsetRowMetaData:
        df=subsetRow(df,rowMetaData,**kwargs)
        mainObject.df=df
    if subsetColMetaData:
        df=subsetCol(df,colMetaData,**kwargs)
        mainObject.df=df

def subsetMain(df,**kwargs):
    for key,values in kwargs.items():
        if key=='delColMain':
            df=df.drop(values,axis=1)
        elif key=='delRowMain':
            df=df.drop(values,axis=0)
    for cl in df.columns:
        df[cl]=pd.to_numeric(df[cl])
    return df

#row_operator is list for operators. can be -1, 0 or 1, implying <, = or > repectively. For category, 0 means not in and 1 in.
#row_threshold is the list of thresholds for rows with which everything will be compared
#For category, row_threshold will have list corresponding to that entry, and for cont. variable, a number.
#For eg., [['Path1','Path2'],10,5]. 
#row_list is list for selections on features (row-wise)
def subsetRow(df,rowMeta,**kwargs):
    row_operator=None
    row_threshold=None
    row_list=None
    for key,values in kwargs.items():
        if key=='row_operator':
            row_operator=values
        elif key=='row_threshold':
            row_threshold=values
        elif key=='row_list':
            row_list=values
    if row_operator is not None and row_threshold is not None and row_list is not None:
        for i in range(len(row_list)):
            if is_float(row_threshold[i]):
                a=[]
                if row_operator[i]==-1:
                    for row in rowMeta.itertuples(index=True, name='Pandas'):
                        if int(getattr(row,row_list[i]))<row_threshold[i]:
                            a.append(row.Index)
                    rowMeta=rowMeta.loc[a,:]
                elif row_operator[i]==0:
                    for row in rowMeta.itertuples(index=True, name='Pandas'):
                        if int(getattr(row,row_list[i]))==row_threshold[i]:
                            a.append(row.Index)
                    rowMeta=rowMeta.loc[a,:]
                else:
                    for row in rowMeta.itertuples(index=True, name='Pandas'):
                        if int(getattr(row,row_list[i]))>row_threshold[i]:
                            a.append(row.Index)
                    rowMeta=rowMeta.loc[a,:]
            else:
                if row_operator==0:
                    a=rowMeta.index
                    for row in rowMeta.itertuples(index=True, name='Pandas'):
                        if getattr(row,row_list[i]) in row_threshold[i]:
                            a.remove(row.Index)
                    rowMeta=rowMeta.loc[a,:]
                else:
                    a=[]
                    for row in rowMeta.itertuples(index=True, name='Pandas'):
                        if getattr(row,row_list[i]) in row_threshold[i]:
                            a.append(row.Index) 
                    rowMeta=rowMeta.loc[a,:]
        genesToSelect=rowMeta.index
        df=df.loc[genesToSelect,:]
    else:
        raise ValueError("None of row_operator, row_threshold and row_list should be null for row MetaData based subsetting")
    return df

#col_operator is an integer. can be 0 or 1, implying not in and in repectively     
#col_name is name of column to be used for selections (like samples or time or anything like that)
#col_list is list of corresponding values (like C1, C2 while using cohorts for subsetting etc) for col_name. It is optional. Its first entry should be the name of the column. 
def subsetCol(df,colMeta,**kwargs):
    col_operator=None
    col_name=None
    col_list=None
    for key,values in kwargs.items():
        if key=='col_operator':
            col_operator=values
        elif key=='col_name':
            col_name=values
        elif key=='col_list':
            col_list=values
    if col_operator is not None and col_name is not None:
        if col_list is None:
            colToSelect=colMeta[col_name]
        else:
            colToSelect=[]
            colName=col_list[0]
            col_list.remove(colName)
            for row in colMeta.itertuples(index=True, name='Pandas'):
                if getattr(row,colName) in col_list:
                    colToSelect.append(getattr(row,col_name))
        if col_operator==1:
            df=df[colToSelect]
        else:
            df=df.drop(colToSelect,axis=1)
    else:
        raise ValueError("col_operator and col_name should not be null for column MetaData based subsetting")
    return df