import plotly
from pipeline.mainClass import mainClass
from plotly.graph_objs import Scatter, Layout

def scatterPlot(mainObject,feature,x,y,**kwargs):
    df = mainObject.df
    unique_val = list(df[feature].unique())
    plotly.offline.iplot({
        'data': [   
            {
                'x': df[df[feature]==val][x],
                'y': df[df[feature]==val][y],
                'name': str(val), 'mode': 'markers',
            } for val in unique_val
        ],
        'layout': {
            'xaxis': {'title': x},
            'yaxis': {'title': y}
        }
    })
