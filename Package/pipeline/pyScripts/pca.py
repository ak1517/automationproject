from sklearn.decomposition import PCA
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from pipeline.mainClass import mainClass
from pipeline.pyScripts.outlier_detect import outlier_detect

def getMap(df,col):
    if col is None:
        n=len(df[0])
        a=[]
        for i in range(n):
            a.append(0)
    else:
        b=list(df[col].unique())
        n=len(b)
        c=list(df[col])
        a=[]
        for x in c:
            for i in range(n):
                if x==b[i]:
                    a.append(i)
                    break
    return a

def unique(list1):       
    list_set = set(list1) 
    unique_list = (list(list_set)) 
    return unique_list

def pca(mainObject,**kwargs):
    nComp=2
    
    data=mainObject.df.T
    metaData=mainObject.colMetaData
    column=None
    for key, value in kwargs.items(): 
        if(key=='nComp'):
            nComp=value
        elif(key=='column'):
            column=value
    pcaModel = PCA(n_components=nComp)
    principalComponents = pcaModel.fit_transform(data)
    colNames = []
    for i in range(1,nComp+1):
        colNames.append('PC_'+str(i))
    principalDf = pd.DataFrame(data = principalComponents
            , columns = colNames)
    if metaData is not None and column is not None:
        fig = plt.figure()
        a=getMap(metaData,column)
        if nComp == 2:
            N=len(unique(a))
            cmap = plt.cm.jet
            cmaplist = [cmap(i) for i in range(cmap.N)]
            labels=list(metaData[column].unique())
            loc = np.linspace(0,N,N+1)
            norm = mpl.colors.BoundaryNorm(loc, cmap.N)
            cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
            fig = plt.figure(figsize=(8,8))
            plt.scatter(principalDf['PC_1'], principalDf['PC_2'], c=a, cmap=cmap,norm=norm)
            cb = plt.colorbar()
            cb.set_ticks(loc)
            cb.set_ticklabels(labels)
        elif nComp == 3:
            ax = fig.add_subplot(111, projection='3d')
            N=len(unique(a))
            cmap = plt.cm.jet
            cmaplist = [cmap(i) for i in range(cmap.N)]
            cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
            ax.scatter(principalDf['PC_1'], principalDf['PC_2'], principalDf['PC_3'], c=a, cmap=cmap)
        else:
            print("nComp should be 2 or 3 for plot.")
    grouped_df = pd.DataFrame()
    for i in metaData[column].unique() :
        new = metaData[metaData[column]== i].index.values
        grouped_df= mainObject.df[new]
        print("When Cohort is "+i)
        index_val = [mainObject.df.columns.get_loc(i) for i in new]
        principal = principalDf.iloc[index_val,:]
        # outlier_detect(grouped_df,principal,threshold=2)
    principalDf.index = mainObject.df.columns
    return principalDf

