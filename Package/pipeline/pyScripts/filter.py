
def filter(mainObject,**kwargs):
    df = mainObject.df
    col_names,col_thresholds,col_operators = None,None,None
    for key,values in kwargs.items():
        if key=='col_names':
            col_names=values
        elif key=='col_thresholds':
            col_thresholds=values
        elif key=='col_operators':
            col_operators=values
    if col_names is None or col_thresholds is None:
        raise KeyError("col_names and col_thresholds are compulsory arguments for filter method")
    if len(col_names)!=len(col_thresholds):
        raise ValueError("col_names and col_threshold should have same length")
    if col_operators is not None:
        if len(col_operators)!=len(col_names):
            raise ValueError("col_names and col_operators should have same length")
    else:
        col_operators = []
        for i in range(len(col_names)):
            col_operators.append('<')
    for i in range(len(col_names)):
        if col_operators=='<':
            df = df[df[col_names[i]]<col_thresholds[i]]
        else:
            df = df[df[col_names[i]]>col_thresholds[i]]
    mainObject.df = df
    return df