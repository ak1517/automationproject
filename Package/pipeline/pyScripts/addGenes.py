import pandas as pd

def removeDuplicates(self,mainObject,column,**kwargs):
    df = mainObject.df 
    df.drop_duplicates(subset = column, inplace = True)
    mainObject.df = df
    return df

def addGenes(self,mainObject,gene_file,gene_column,common_column,**kwargs):
    df = mainObject.df
    df[common_column] = df.index
    genes = pd.read_csv(gene_file)
    merged_gene = pd.merge(df,genes,on = common_column)
    mainObject.df = merged_gene
    removeDuplicates().process(mainObject,gene_column)
    mainObject.df.index = mainObject.df[gene_column]
    return mainObject.df