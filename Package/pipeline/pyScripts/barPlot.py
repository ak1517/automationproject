import matplotlib.pyplot as plt 
from pipeline.mainClass import mainClass

def barPlot(mainObject,**kwargs):
    df = mainObject.df
    for key,value in kwargs.items():
        if key == 'columns':
            df = df.loc[:,value]
            break
    barplot = df.plot.bar()
    plt.show(barplot)
