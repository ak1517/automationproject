import pandas as pd
from pipeline.mainClass import mainClass
import nbformat as nbf
from os import listdir
from os.path import isfile, join
from cmapPy.pandasGEXpress.parse import parse
import yaml

class generator_m():

    def getArgsRFile(self,x,cwdPath):
        filePath = cwdPath+"/pipeline/Rscripts/"+ x + ".R"
        f = open(filePath,"r")
        for s in f:
            if s.startswith("perform_"+x):
                s = s.split("(")
                s = s[1].split(")")
                s = s[0].split(",")
                for i in range(len(s)):
                    s[i] = s[i].strip()
                break
        if isinstance(s,list) is False:
            raise ValueError("File "+x+".R does not contain appropriate function definition")
        return s

    def getArgsPython(self,x,name):
        s=[]
        for key,values in x.items():
            if key!="perform" and key!="dataName" :
                if(isinstance(values,str)):
                    s.append(key+" = '"+str(values)+"'")
                else:
                    s.append(key+" = "+str(values))
        a=name
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def putPython(self,name):
        a="df_RPy = "+name+".df\n"
        a+="metadata = "+name+".colMetaData"
        return a

    def getPython(self,i,x,name):
        a="""\
try:
    inPlace = params["""+str(i+1)+"]"+"['"+x+"""']['inPlace']
except:
    inPlace = True
if inPlace:
    """+name+""".df = df_RPy"""
        return a

    def getPythonWF(self):
        a="mainObject.df = df_RPy"
        return a
    
    def getArgsRWF(self,x,params,operation):
        s=[]
        for it in x:
            try:
                if it=='df_RPy':
                    continue
                if it=='metadata':
                    s.append(it+" = "+it)
                    continue
                z=params[it]
                if(isinstance(z,str)):
                    s.append(it+" = '"+str(z)+"'")
                else:
                    s.append(it+" = "+str(z))
            except:
                raise KeyError(it+" is compulsory argument for "+operation)
        a="df_RPy = df_RPy" 
        for i in range(len(s)):
            a+=","+s[i]
        return a

    def putR(self):
        a="%put df_RPy\n"
        return a

    def getR(self):
        a="%get df_RPy\n%get metadata\n"
        return a

    def getImportStatements(self):
        code="""\
from pipeline.mainClass import mainClass
from pipeline.user import automate
from pipeline.multiple import multiple
import plotly
import matplotlib
plotly.offline.init_notebook_mode()"""
        return code

    def getMainObject(self,name,i,yamlFilePath):
        code="""\
params = automate().getListFromYaml('"""+yamlFilePath+"""')
"""+name+""" = multiple().get_data('"""+name+"""',"""+i+""",params)"""
        return code
        
    def getDriveGCT(self,yamlFilePath,name,x):
        cells = []
        code="""\
library('googledrive')
td <- team_drive_get('"""+x[0]+"""')
file_drive_path <- file.path( '"""
        code+=x[1]+"'"
        for i in range(2,len(x)):
            code+=", '"+x[i]+"'"
        code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_"+name+" <- drive_download(file_drive_metadata, overwrite = T)"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "R"}
        cells.append(cell)
        code="""\
params = automate().getListFromYaml('"""+yamlFilePath+"""')
"""+name+""" = automate().get_data_gct('"""+x[-1]+"')"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "SoS"}
        cells.append(cell)
        return cells

    def getDriveAll(self,yamlFilePath,name,mainX=None,rowX=None,colX=None):
        cells=[]
        code = "library('googledrive')\ntd <- team_drive_get('"+mainX[0]+"')\n\n"
        if mainX is not None:
            code+="file_drive_path <- file.path( '"
            code+=mainX[1]+"'"
            for i in range(2,len(mainX)):
                code+=", '"+mainX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_"+name+"_main <- drive_download(file_drive_metadata, overwrite = T)\n\n"
        if rowX is not None:
            code+="file_drive_path <- file.path( '"
            code+=rowX[1]+"'"
            for i in range(2,len(rowX)):
                code+=", '"+rowX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_"+name+"_rowMD <- drive_download(file_drive_metadata, overwrite = T)\n\n"
        if colX is not None:
            code+="file_drive_path <- file.path( '"
            code+=colX[1]+"'"
            for i in range(2,len(colX)):
                code+=", '"+colX[i]+"'"
            code+=")\nfile_drive_metadata <- drive_get(file_drive_path, team_drive = td)\ndownloaded_file_"+name+"_colMD <- drive_download(file_drive_metadata, overwrite = T)"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "R"}
        cells.append(cell)
        code = "params = automate().getListFromYaml('"+yamlFilePath+"')\n"
        a = []
        if mainX is not None:
            a.append("mainCSV = '"+mainX[-1]+"'")
        if rowX is not None:
            a.append("rowCSV = '"+rowX[-1]+"'")
        if colX is not None:
            a.append("colCSV = '"+colX[-1]+"'")
        s = a[0]
        for i in range(1,len(a)):
            s+=", "+a[i]
        # code += ""+name+" = automate().get_data_all("+s+")\nmetadata = "+name+".colMetaData"
        code += ""+name+" = automate().get_data_all("+s+")"
        cell = nbf.v4.new_code_cell(code)
        cell['metadata'] = {"kernel": "SoS"}
        cells.append(cell)
        return cells
    
    def getDriveFile(self,yamlFilePath,dataName,params):
        gct=None
        mainCSV=None
        rowCSV=None
        colCSV=None
        for key,values in params.items():
            if key=='gct':
                gct=values
            elif key=='mainCSV':
                mainCSV=values
            elif key=='rowCSV':
                rowCSV=values
            elif key=='colCSV':
                colCSV=values
        if gct is not None:
            cells = self.getDriveGCT(yamlFilePath,dataName,gct)
        else:
            cells = self.getDriveAll(yamlFilePath,dataName,mainX=mainCSV,rowX=rowCSV,colX=colCSV)
        return cells

    def getFunctions(self,apply,rScripts,pyScripts,cwdPath):
        cells = []
        apply = set(apply)
        apply = list(apply)
        for x in apply:
            if x in rScripts:                
                cells.append(nbf.v4.new_markdown_cell("### Importing function "+x))
                filePath = cwdPath+"/pipeline/Rscripts/"+ x + ".R"
                f = open(filePath,"r")
                s=""
                for x in f:
                    if x.startswith("#"):
                        continue
                    s+=x
                cell=nbf.v4.new_code_cell(s)
                cell['metadata']={"kernel": "R"}
                cells.append(cell)  
            elif x in pyScripts:
                cells.append(nbf.v4.new_markdown_cell("### Importing function "+x))
                filePath = cwdPath+"/pipeline/pyScripts/"+ x + ".py"
                f = open(filePath,"r")
                s=""
                for x in f:
                    if x.startswith("#"):
                        continue
                    s+=x
                cell=nbf.v4.new_code_cell(s)
                cell['metadata']={"kernel": "SoS"}
                cells.append(cell)  
            else:
                raise KeyError("Function "+x+" neither in R nor in python folder")
        return cells
    
    def generateNotebook(self,apply,data_list,params,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath):
        nb=nbf.v4.new_notebook()
        nb['metadata']={
            "kernelspec": {
                "display_name": "SoS",
                "language": "sos",
                "name": "sos"
            },
            "language_info": {
                "codemirror_mode": "sos",
                "file_extension": ".sos",
                "mimetype": "text/x-sos",
                "name": "sos",
                "nbconvert_exporter": "sos_notebook.converter.SoS_Exporter",
                "pygments_lexer": "sos"
            }
        }
        cells=[]
        cells.append(nbf.v4.new_markdown_cell("### Importing libraries"))
        cells.append(nbf.v4.new_code_cell(self.getImportStatements()))
        cells.append(nbf.v4.new_code_cell("%matplotlib inline"))
        cells.append(nbf.v4.new_markdown_cell("### Setting things up...(Importing data and other necessities)"))
        for i in range(2,len(data_list)+2):
            for key,values in params[i].items():
                if key== data_list[i-2]:
                    if params[i][ data_list[i-2]]['from'] =='disk':
                        cells.append(nbf.v4.new_code_cell(self.getMainObject(data_list[i-2],str(i),yamlFilePath)))
                    elif params[i][ data_list[i-2]]['from'] =='drive':
                        moreCells = self.getDriveFile(yamlFilePath,data_list[i-2],params[i][data_list[i-2]])
                        for x in moreCells:
                            cells.append(x)
                    else:
                        raise ValueError("data['from'] should either be 'disk' or 'drive'")
                    cells.append(nbf.v4.new_code_cell(data_list[i-2]+".df.head()"))
                    cells.append(nbf.v4.new_code_cell(data_list[i-2]+".colMetaData.head()"))
        del params[0:len(data_list)+2]
        del apply[0:len(data_list)+2]
        temp = self.getFunctions(apply,rScripts,pyScripts,cwdPath)
        for x in temp:
            cells.append(x)
        for i in range(len(apply)):
            if apply[i] in rScripts:
                cells.append(nbf.v4.new_markdown_cell("### Implementing "+apply[i]))
                args = self.getArgsRFile(apply[i],cwdPath)
                code=self.putPython(params[i][apply[i]]['dataName'])
                cells.append(nbf.v4.new_code_cell(code))
                code=self.putR()+self.getR()
                code+="df_RPy = perform_"+apply[i]+"("+self.getArgsRWF(args,params[i][apply[i]],apply[i])+")\n"
                code+="head(df_RPy)"
                cell=nbf.v4.new_code_cell(code)
                cell['metadata']={"kernel": "R"}
                cells.append(cell)  
                code=self.getPython(i,apply[i],params[i][apply[i]]['dataName'])
                cell=nbf.v4.new_code_cell(code)
                cell['metadata']={"kernel": "SoS"}
                cells.append(cell)                  
            else:
                cells.append(nbf.v4.new_markdown_cell("### Implementing "+apply[i]))
                code=""
                if apply[i] == 'merge':
                    code+= params[i][apply[i]]['dataName']+' = ' + apply[i]+"("+params[i][apply[i]]['data1']+','+params[i][apply[i]]['data2']+")"
                else:
                    code+=apply[i]+"("+self.getArgsPython(params[i][apply[i]],params[i][apply[i]]['dataName'])+")"
                cell=nbf.v4.new_code_cell(code)
                cell['metadata']={"kernel": "SoS"}
                cells.append(cell) 
                
        nb['cells']=cells
        nbf.write(nb,notebookName)

class automate():
    
    def getApply(self,yamlList):
        a=[]
        for x in yamlList:
            for key,values in x.items():
                take=False
                for key2,values2 in values.items():
                    if values2 is not None:
                        take=True
                if take:
                    a.append(key)
        return a
    
    def getListFromYaml(self,fileAddress):
        return yaml.load(open(fileAddress),Loader=yaml.SafeLoader)
        
    def getRScripts(self,mypath):
        a = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        b=[]
        for x in a:
            b.append(x.split('.'))
        c=[]
        for i in range(len(b)):
            if b[i][1]=='R':
                c.append(b[i][0])
        return c

    def getPyScripts(self,mypath):
        a = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        b=[]
        for x in a:
            b.append(x.split('.'))
        c=[]
        for i in range(len(b)):
            if b[i][1]=='py':
                c.append(b[i][0])
        return c

    def intelligence(self,constraints,apply):
        for key,values in constraints.items():
            for key2, values2 in values.items():
                if key2 in apply:
                    if apply.index(key2) > apply.index(key) :
                        continue
                    else :
                        raise ValueError("Please check your order, cannot perform "+key2 +" before "+ key)

    

class multiple():

    def get_data_all(self,mainCSV = None,rowCSV = None,colCSV = None):
        mainObject=mainClass()
        if mainCSV is not None:
            mainObject.setDF(pd.read_csv(mainCSV,index_col=0))
        if rowCSV is not None:
            mainObject.setRMD(pd.read_csv(rowCSV,index_col=0))
        if colCSV is not None:
            mainObject.setCMD(pd.read_csv(colCSV,index_col=0))
            df = mainObject.df
            mainObject.setDF(df[mainObject.colMetaData.index.values])
        return mainObject

    def get_data_gct(self,gct):
        mainObject=mainClass()        
        x = parse(gct)
        mainObject.setDF(x.data_df)
        mainObject.setRMD(x.row_metadata_df)
        mainObject.setCMD(x.col_metadata_df)
        x.row_metadata_df.to_csv("results/row_meta_data.csv")
        x.data_df.to_csv("results/main_data.csv")
        x.col_metadata_df.to_csv("results/col_meta_data.csv")
        return mainObject
    
    def get_data(self,name,i,list):
        mainObject=mainClass()
        gct=None
        mainCSV=None
        rowCSV=None
        colCSV=None
        for key,values in list[int(i)][name].items():
            if key=='gct':
                gct=values
            elif key=='mainCSV':
                mainCSV=values
            elif key=='rowCSV':
                rowCSV=values
            elif key=='colCSV':
                colCSV=values
        if gct is not None:
            x = parse(gct)
            mainObject.setDF(x.data_df)
            mainObject.setRMD(x.row_metadata_df)
            mainObject.setCMD(x.col_metadata_df)

            x.row_metadata_df.to_csv("results/row_meta_data.csv")
            x.data_df.to_csv("results/main_data.csv")
            x.col_metadata_df.to_csv("results/col_meta_data.csv")
        else:
            if mainCSV is not None:
                mainObject.setDF(pd.read_csv(mainCSV,index_col=0))
            if rowCSV is not None:
                mainObject.setRMD(pd.read_csv(rowCSV,index_col=0))
            if colCSV is not None:
                mainObject.setCMD(pd.read_csv(colCSV,index_col=0))
                df = mainObject.df
                mainObject.setDF(df[mainObject.colMetaData.index.values])
        return mainObject

    def Multipledata(self,params,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath):
        data_list = []
        num = 0
        for i in params[0]['data']['dataNames'] :
            data_list.append(i)
        apply=automate().getApply(params)
        generator_m().generateNotebook(apply,data_list,params,rScripts,pyScripts,notebookName,yamlFilePath,cwdPath)
        
            
