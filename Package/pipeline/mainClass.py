from abc import ABC, abstractmethod 

class mainClass():

    def __init__(self):
        self.df=None
        self.rowMetaData=None
        self.colMetaData=None

    def setDF(self,df):
        self.df=df
    
    def setRMD(self,rmd):
        self.rowMetaData=rmd
    
    def setCMD(self,cmd):
        self.colMetaData=cmd
